#include "Neptunien.h"
#include <stdlib.h>
#include "define.h"

Neptunien::Neptunien(int type,int valeur):ExtraTerrestre(type,valeur){}

void Neptunien::resetExtraTerrestre(){
	coord.setPositionX(rand()%(LARGEUR_TERRAIN-2)+1);
	coord.setPositionY(rand()%((LARGEUR_TERRAIN-2)/3-2)+(HAUTEUR_TERRAIN-(LARGEUR_TERRAIN-2)/3));
	isAlive = true;
}