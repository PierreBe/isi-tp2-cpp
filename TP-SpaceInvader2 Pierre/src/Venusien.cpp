#include "Venusien.h"
#include <stdlib.h>
#include "define.h"

Venusien::Venusien(int type,int valeur):ExtraTerrestre(type,valeur){}

void Venusien::moveVenusien(){
	removeExtraTerrestre();
	if(goRight&&coord.getPositionX()!=LARGEUR_TERRAIN-1){
		coord.setPositionX(coord.getPositionX()+1);
	}else if(!goRight&&coord.getPositionX()!=1){
		coord.setPositionX(coord.getPositionX()-1);
	}else{
		coord.setPositionY(coord.getPositionY()+1);
		goRight=!goRight;
	}
	putExtraTerrestre();
}

void Venusien::resetExtraTerrestre(){
	coord.setPositionX(rand()%(LARGEUR_TERRAIN-2)+1);
	coord.setPositionY(rand()%5+21);
	isAlive = true;
}