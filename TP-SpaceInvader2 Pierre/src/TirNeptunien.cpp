#include "TirNeptunien.h"
#include <iostream>
#include <stdlib.h>
#include "define.h"

void TirNeptunien::startLaser(unsigned short x,unsigned short y){
	if(x==1){
		droite=1;
	}else if(x==LARGEUR_TERRAIN-1){
		droite=0;
	}else{
		droite=rand()%2;
	}

	if(droite){
		coord.setPositionX(x+1);
	}else{
		coord.setPositionX(x-1);
	}
	coord.setPositionY(y+1);
	putLaser();
	isAlive=true;
}

void TirNeptunien::putLaser()const{
	coord.gotoXY(coord.getPositionX(),coord.getPositionY());
	if(droite){
		std::cout<<styleLaserDroite;
	}else{
		std::cout<<styleLaserGauche;
	}
}

void TirNeptunien::moveLaser(){
	auto posX=coord.getPositionX();
	auto posY=coord.getPositionY();
	removeLaser();
	if(posX>1&&posX<LARGEUR_TERRAIN-1&&posY<HAUTEUR_TERRAIN-1){
		if(droite){
			coord.setPositionX(coord.getPositionX()+1);
		}else{
			coord.setPositionX(coord.getPositionX()-1);
		}
		coord.setPositionY(coord.getPositionY()+1);
		putLaser();
	}
	else {
		isAlive=false;
	}
}