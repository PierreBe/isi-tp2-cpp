// Bibliothèques :
#include <time.h>
#include <math.h>
#include "UIKit.h"
#include "define.h"
#include "Timer.h"
#include "Vaisseau.h"
#include "Laser.h"
#include "Martien.h"
#include "TirMartien.h"
#include "Venusien.h"
#include "TirVenusien.h"
#include "Neptunien.h"
#include "TirNeptunien.h"

//#define DEBUG

// Prototypes :
unsigned int menu();
void aide();
void gameOver(unsigned short score,unsigned short limiteTemps);
bool ennemiTouche(Laser* laser,ExtraTerrestre* extraTerrestre,unsigned short* score);

int main(){
	srand (time(NULL));
	UIKit::curseurVisible(false);

	// Déclarations :
	unsigned short limiteTemps;
	unsigned short difficulte;
	unsigned short score;
	Timer timerPartie;

	Vaisseau ship;
	unsigned short vie;
	Timer timerShip;
	Laser* laser;
	unsigned short nbLasers;
	unsigned short nbLasersMax;
	Timer timerLaser;

	Martien** martien;
	unsigned short nbMartiens;
	unsigned short nbMartiensMax;
	TirMartien* tirMartien;
	Timer timerApparitionMartien;
	Timer timerJiggleMartien;
	unsigned short probaTirMartien;
	Timer timerTirMartien;

	Venusien** venusien;
	unsigned short nbVenusiens;
	unsigned short nbVenusiensMax;
	TirVenusien* tirVenusien;
	Timer timerApparitionVenusien;
	Timer timerMoveVenusien;
	unsigned short probaTirVenusien;
	Timer timerTirVenusien;

	Neptunien** neptunien;
	unsigned short nbNeptuniens;
	unsigned short nbNeptuniensMax;
	TirNeptunien* tirNeptunien;
	Timer timerApparitionNeptunien;
	unsigned short probaTirNeptunien;
	Timer timerTirNeptunien;

	do{

		// Menu :
		difficulte=menu();
		if(difficulte==0){
			vie=0;
			nbMartiensMax=0;
		}else if(difficulte==1){
			limiteTemps=180;
			vie=3;
			timerShip.set(50);
			nbLasersMax=10;
			timerLaser.set(30);

			nbMartiensMax=6;
			timerApparitionMartien.set(500);
			timerJiggleMartien.set(1000);
			probaTirMartien=50;
			timerTirMartien.set(90);

			nbVenusiensMax=3;
			timerApparitionVenusien.set(2000);
			timerMoveVenusien.set(800);
			probaTirVenusien=50;
			timerTirVenusien.set(200);

			nbNeptuniensMax=2;
			timerApparitionNeptunien.set(500);
			probaTirNeptunien=10;
			timerTirNeptunien.set(200);

		}else if(difficulte==2){
			limiteTemps=240;
			vie=2;
			timerShip.set(70);
			nbLasersMax=5;
			timerLaser.set(50);

			nbMartiensMax=15;
			timerApparitionMartien.set(300);
			timerJiggleMartien.set(500);
			probaTirMartien=40;
			timerTirMartien.set(75);

			nbVenusiensMax=7;
			timerApparitionVenusien.set(1300);
			timerMoveVenusien.set(650);
			probaTirVenusien=40;
			timerTirVenusien.set(150);

			nbNeptuniensMax=5;
			timerApparitionNeptunien.set(500);
			probaTirNeptunien=10;
			timerTirNeptunien.set(200);

		}else if(difficulte==3){
			limiteTemps=300;
			vie=1;
			timerShip.set(90);
			nbLasersMax=3;
			timerLaser.set(70);

			nbMartiensMax=10;
			timerApparitionMartien.set(100);
			timerJiggleMartien.set(250);
			probaTirMartien=2;
			timerTirMartien.set(60);

			nbVenusiensMax=9;
			timerApparitionVenusien.set(900);
			timerMoveVenusien.set(500);
			probaTirVenusien=80;
			timerTirVenusien.set(100);

			nbNeptuniensMax=5;
			timerApparitionNeptunien.set(500);
			probaTirNeptunien=10;
			timerTirNeptunien.set(200);

		}else{
			return 1;
		}

		// Initialisations :
		score=0;

		ship.coord.setPositionX(LARGEUR_TERRAIN/2);
		laser=new Laser[nbLasersMax];
		nbLasers=0;

		martien=new Martien*[nbMartiensMax];
		nbMartiens=0;
		tirMartien=new TirMartien[nbMartiensMax];
		for(unsigned short i=0;i<nbMartiensMax;i++){
			tirMartien[i].isAlive=false;
		}

		venusien=new Venusien*[nbVenusiensMax];
		nbVenusiens=0;
		tirVenusien=new TirVenusien[nbVenusiensMax];
		for(unsigned short i=0;i<nbVenusiensMax;i++){
			tirVenusien[i].isAlive=false;	// on pourrait l'enlever en mettant ça dans le constructeur
		}

		neptunien=new Neptunien*[nbNeptuniensMax];
		nbNeptuniens=0;
		tirNeptunien=new TirNeptunien[nbNeptuniensMax];
		for(unsigned short i=0;i<nbNeptuniensMax;i++){
			tirNeptunien[i].isAlive=false;	// on pourrait l'enlever en mettant ça dans le constructeur
		}

		// Setup environnement de jeu :
		system("cls");
		UIKit::cadre(0,0,LARGEUR_TERRAIN,HAUTEUR_TERRAIN,BLANC);
		ship.modifierPosition(0);
		UIKit::gotoXY(LARGEUR_TERRAIN+2,1);
		std::cout<<" Temps :";
		UIKit::gotoXY(LARGEUR_TERRAIN+2+9,1);
		std::cout<<limiteTemps<<' '<<' ';
		UIKit::gotoXY(LARGEUR_TERRAIN+2,3);
		std::cout<<" Score :";
		UIKit::gotoXY(LARGEUR_TERRAIN+2+9,3);
		std::cout<<score<<' ';
		UIKit::gotoXY(LARGEUR_TERRAIN+2,5);
		std::cout<<"   Vie :";
		UIKit::gotoXY(LARGEUR_TERRAIN+2+9,5);
		std::cout<<vie<<"/"<<vie;
		UIKit::gotoXY(LARGEUR_TERRAIN+2,7);
		std::cout<<"Lasers :";
		UIKit::gotoXY(LARGEUR_TERRAIN+2+9,7);
		std::cout<<nbLasersMax-nbLasers<<"/"<<nbLasersMax<<' ';

		UIKit::color(ROUGE);
		UIKit::gotoXY(LARGEUR_TERRAIN+2,11);
		std::cout<<"  Martiens :";
		UIKit::gotoXY(LARGEUR_TERRAIN+2+13,11);
		std::cout<<nbMartiens<<"/"<<nbMartiensMax<<' ';
		UIKit::color(VERT);
		UIKit::gotoXY(LARGEUR_TERRAIN+2,13);
		std::cout<<" Venusiens :";
		UIKit::gotoXY(LARGEUR_TERRAIN+2+13,13);
		std::cout<<nbVenusiens<<"/"<<nbVenusiensMax<<' ';
		UIKit::color(BLEU);
		UIKit::gotoXY(LARGEUR_TERRAIN+2,15);
		std::cout<<"Neptuniens :";
		UIKit::gotoXY(LARGEUR_TERRAIN+2+13,15);
		std::cout<<nbNeptuniens<<"/"<<nbNeptuniensMax<<' ';

		UIKit::color(BLANC);
		UIKit::gotoXY(LARGEUR_TERRAIN+2,HAUTEUR_TERRAIN-9);
		std::cout<<"<D>    Gauche";
		UIKit::gotoXY(LARGEUR_TERRAIN+2,HAUTEUR_TERRAIN-7);
		std::cout<<"<A>    Droite";
		UIKit::gotoXY(LARGEUR_TERRAIN+2,HAUTEUR_TERRAIN-5);
		std::cout<<"<SPACE> Tirer";
		UIKit::gotoXY(LARGEUR_TERRAIN+2,HAUTEUR_TERRAIN-3);
		std::cout<<"<P>     Pause";
		UIKit::gotoXY(LARGEUR_TERRAIN+2,HAUTEUR_TERRAIN-1);
		std::cout<<"<ESC>  Retour";

#ifdef DEBUG
		unsigned int noFrame=0;
		bool pas_a_pas=0;
		UIKit::gotoXY(LARGEUR_TERRAIN+2,HAUTEUR_TERRAIN-19);
		std::cout<<"*** DEBUG ***";
		UIKit::gotoXY(LARGEUR_TERRAIN+2,HAUTEUR_TERRAIN-17);
		std::cout<<"Frame :";
		UIKit::gotoXY(LARGEUR_TERRAIN+2,HAUTEUR_TERRAIN-15);
		std::cout<<"<T> Pas-à-pas";
		UIKit::gotoXY(LARGEUR_TERRAIN+2,HAUTEUR_TERRAIN-13);
		std::cout<<"*** DEBUG ***";
#endif

		// Jeu :
		timerPartie.set(1000);
		while(!GetKeyState(27)&&vie&&limiteTemps){

			// Afficher le temps :
			if(timerPartie.isReady()){
				limiteTemps--;
				UIKit::gotoXY(LARGEUR_TERRAIN+2+9,1);
				std::cout<<limiteTemps<<' '<<' ';
			}

#ifdef DEBUG
			if(GetKeyState('T')){
				pas_a_pas=!pas_a_pas;
				UIKit::gotoXY(0,HAUTEUR_TERRAIN+1);
				std::cout<<"                                              ";
				keybd_event('T', 0, KEYEVENTF_KEYUP, 0);
			}
			if(pas_a_pas){
				UIKit::gotoXY(0,HAUTEUR_TERRAIN+1);
				system("pause");
			}
#endif

			// Pause :
			if(GetAsyncKeyState('P')){
				UIKit::gotoXY(1,HAUTEUR_TERRAIN+1);
				std::cout<<"PAUSE !";
				keybd_event('P', 0, KEYEVENTF_KEYUP, 0);
				while(!GetKeyState('P')&&!GetKeyState(27)){}
				UIKit::gotoXY(1,HAUTEUR_TERRAIN+1);
				std::cout<<"       ";
				keybd_event('P', 0, KEYEVENTF_KEYUP, 0);
				//resetter les timers ou mettre clock() en pause ?
			}

			UIKit::color(ROUGE);
			// Faire bouger les martiens :
			if(timerJiggleMartien.isReady()){
				for(unsigned short i=0;i<nbMartiens;i++){
					if(martien[i]->coord.getPositionX()!=1){martien[i]->jiggleMartien();}
					//controler si il se deplace sur un laser ?
				}
			}
			// Faire spawner les martiens :
			if(timerApparitionMartien.isReady()&&nbMartiens<nbMartiensMax){
				martien[nbMartiens]=new Martien(157,5);
				martien[nbMartiens]->resetExtraTerrestre();
				martien[nbMartiens]->putExtraTerrestre();
				nbMartiens++;
				UIKit::gotoXY(LARGEUR_TERRAIN+2+13,11);
				std::cout<<nbMartiens<<"/"<<nbMartiensMax<<' ';
			}
			// Faire tirer les martiens :
			if(timerTirMartien.isReady()){
				for(unsigned short i=0;i<nbMartiensMax;i++){
					if(tirMartien[i].isAlive){
						tirMartien[i].moveLaser();
						if(tirMartien[i].coord.getPositionX()==ship.coord.getPositionX()&&tirMartien[i].coord.getPositionY()==ship.coord.getPositionY()){
							tirMartien[i].killLaser();
							ship.modifierPosition(0);
							vie--;
							UIKit::color(BLANC);
							UIKit::gotoXY(LARGEUR_TERRAIN+2+9,5);
							std::cout<<vie;
							UIKit::color(ROUGE);
						}
					}else if(i<nbMartiens&&rand()%probaTirMartien==0){
						tirMartien[i].startLaser(martien[i]->coord.getPositionX(),martien[i]->coord.getPositionY());
					}
				}
			}

			UIKit::color(VERT);
			// Faire bouger les venusiens :
			if(timerMoveVenusien.isReady()){
				for(unsigned short i=0;i<nbVenusiens;i++){
					venusien[i]->moveVenusien();
					//controler si il se deplace sur un laser ?
				}
			}
			// Faire spawner les venusiens :
			if(timerApparitionVenusien.isReady()&&nbVenusiens<nbVenusiensMax){
				venusien[nbVenusiens]=new Venusien(197,10);
				venusien[nbVenusiens]->resetExtraTerrestre();
				venusien[nbVenusiens]->putExtraTerrestre();
				nbVenusiens++;
				UIKit::gotoXY(LARGEUR_TERRAIN+2+13,13);
				std::cout<<nbVenusiens<<"/"<<nbVenusiensMax<<' ';
			}
			// Faire tirer les venusiens :
			if(timerTirVenusien.isReady()){
				for(unsigned short i=0;i<nbVenusiensMax;i++){
					if(tirVenusien[i].isAlive){
						tirVenusien[i].moveLaser();
						if(tirVenusien[i].coord.getPositionX()==ship.coord.getPositionX()&&tirVenusien[i].coord.getPositionY()==ship.coord.getPositionY()){
							tirVenusien[i].killLaser();
							ship.modifierPosition(0);
							vie--;
							UIKit::color(BLANC);
							UIKit::gotoXY(LARGEUR_TERRAIN+2+9,5);
							std::cout<<vie;
							UIKit::color(VERT);
						}
					}else if(i<nbVenusiens&&rand()%probaTirVenusien==0){
						tirVenusien[i].startLaser(venusien[i]->coord.getPositionX(),venusien[i]->coord.getPositionY());
					}
				}
			}

			UIKit::color(BLEU);
			// Rafraîchir les neptuniens :
			for(unsigned short i=0;i<nbNeptuniens;i++){
				neptunien[i]->putExtraTerrestre();
			}
			// Faire spawner les neptuniens :
			if(timerApparitionNeptunien.isReady()&&nbNeptuniens<nbNeptuniensMax){
				neptunien[nbNeptuniens]=new Neptunien(207,2);
				neptunien[nbNeptuniens]->resetExtraTerrestre();
				neptunien[nbNeptuniens]->putExtraTerrestre();
				nbNeptuniens++;
				UIKit::gotoXY(LARGEUR_TERRAIN+2+13,15);
				std::cout<<nbNeptuniens<<"/"<<nbNeptuniensMax<<' ';
			}
			// Faire tirer les neptuniens :
			if(timerTirNeptunien.isReady()){
				for(unsigned short i=0;i<nbNeptuniensMax;i++){
					if(tirNeptunien[i].isAlive){
						tirNeptunien[i].moveLaser();
						if(tirNeptunien[i].coord.getPositionX()==ship.coord.getPositionX()&&tirNeptunien[i].coord.getPositionY()==ship.coord.getPositionY()){
							tirNeptunien[i].killLaser();
							ship.modifierPosition(0);
							vie--;
							UIKit::color(BLANC);
							UIKit::gotoXY(LARGEUR_TERRAIN+2+9,5);
							std::cout<<vie;
							UIKit::color(VERT);
						}
					}else if(i<nbNeptuniens&&rand()%probaTirNeptunien==0){
						tirNeptunien[i].startLaser(neptunien[i]->coord.getPositionX(),neptunien[i]->coord.getPositionY());
					}
				}
			}

			UIKit::color(BLANC);
			// Prendre les commandes du vaisseau :
			if(timerShip.isReady()){
				if(GetAsyncKeyState('A')&&UIKit::getCharXY(ship.coord.getPositionX()-1,ship.coord.getPositionY())!=char(186)){
					ship.modifierPosition('k');
				}else if(GetAsyncKeyState('D')&&UIKit::getCharXY(ship.coord.getPositionX()+1,ship.coord.getPositionY())!=char(186)){
					ship.modifierPosition('l');
				}
				if(GetAsyncKeyState(' ')&&nbLasers<nbLasersMax){
					laser[nbLasers].startLaser(ship.coord.getPositionX());
					nbLasers++;
					UIKit::gotoXY(LARGEUR_TERRAIN+2+9,7);
					std::cout<<nbLasersMax-nbLasers<<"/"<<nbLasersMax<<' ';
				}
			}

			// Faire bouger les lasers et gérer leurs collisions :
			if(timerLaser.isReady()&&laser[0].isAlive){
				unsigned short j;
				bool touche;//test
				if(laser[0].coord.getPositionY()==1){
					laser[0].killLaser();
					for(unsigned short i=0;i<nbLasers-1;i++){
						laser[i]=laser[i+1];
					}
					laser[nbLasers-1].isAlive=false;
					nbLasers--;
					UIKit::gotoXY(LARGEUR_TERRAIN+2+9,7);
					std::cout<<nbLasersMax-nbLasers<<"/"<<nbLasersMax<<' ';
				}
				for(unsigned short i=0;i<nbLasers;i++){
					touche=0;
					laser[i].moveLaser();
					// Neptuniens :
					j=0;
					while(!touche&&j<nbNeptuniens&&!ennemiTouche(&laser[i],neptunien[j],&score)){j++;}
					if(!touche&&j<nbNeptuniens){
						for(unsigned short k=i;k<nbLasers-1;k++){ 
							laser[k]=laser[k+1];
						}
						i--;
						laser[nbLasers-1].isAlive=false;
						nbLasers--;
						UIKit::gotoXY(LARGEUR_TERRAIN+2+9,7);
						std::cout<<nbLasersMax-nbLasers<<"/"<<nbLasersMax<<' ';
						delete neptunien[j];
						for(unsigned short k=j;k<nbNeptuniens-1;k++){ 
							neptunien[k]=neptunien[k+1];
						}
						nbNeptuniens--;
						UIKit::gotoXY(LARGEUR_TERRAIN+2+9,3);
						std::cout<<score<<' ';
						UIKit::color(ROUGE);
						UIKit::gotoXY(LARGEUR_TERRAIN+2+13,15);
						std::cout<<nbNeptuniens<<"/"<<nbNeptuniensMax<<' ';
						UIKit::color(BLANC);
						touche=1;
					}
					// Vénusiens :
					j=0;
					while(!touche&&j<nbVenusiens&&!ennemiTouche(&laser[i],venusien[j],&score)){j++;}
					if(!touche&&j<nbVenusiens){
						for(unsigned short k=i;k<nbLasers-1;k++){ 
							laser[k]=laser[k+1];
						}
						i--;
						laser[nbLasers-1].isAlive=false;
						nbLasers--;
						UIKit::gotoXY(LARGEUR_TERRAIN+2+9,7);
						std::cout<<nbLasersMax-nbLasers<<"/"<<nbLasersMax<<' ';
						delete venusien[j];
						for(unsigned short k=j;k<nbVenusiens-1;k++){ 
							venusien[k]=venusien[k+1];
						}
						nbVenusiens--;
						UIKit::gotoXY(LARGEUR_TERRAIN+2+9,3);
						std::cout<<score<<' ';
						UIKit::color(VERT);
						UIKit::gotoXY(LARGEUR_TERRAIN+2+13,13);
						std::cout<<nbVenusiens<<"/"<<nbVenusiensMax<<' ';
						UIKit::color(BLANC);
						touche=1;
					}
					// Martiens :
					j=0;
					while(!touche&&j<nbMartiens&&!ennemiTouche(&laser[i],martien[j],&score)){j++;}
					if(!touche&&j<nbMartiens){
						for(unsigned short k=i;k<nbLasers-1;k++){ 
							laser[k]=laser[k+1];
						}
						i--;
						laser[nbLasers-1].isAlive=false;
						nbLasers--;
						UIKit::gotoXY(LARGEUR_TERRAIN+2+9,7);
						std::cout<<nbLasersMax-nbLasers<<"/"<<nbLasersMax<<' ';
						delete martien[j];
						for(unsigned short k=j;k<nbMartiens-1;k++){ 
							martien[k]=martien[k+1];
						}
						nbMartiens--;
						UIKit::gotoXY(LARGEUR_TERRAIN+2+9,3);
						std::cout<<score<<' ';
						UIKit::color(BLEU);
						UIKit::gotoXY(LARGEUR_TERRAIN+2+13,11);
						std::cout<<nbMartiens<<"/"<<nbMartiensMax<<' ';
						UIKit::color(BLANC);
						touche=1;
					}
				}
			}

			// Fin de la boucle de jeu :
			UIKit::color(BLANC);
#ifdef DEBUG
			UIKit::gotoXY(LARGEUR_TERRAIN+2+8,HAUTEUR_TERRAIN-17);
			std::cout<<noFrame;
			noFrame++;
#endif

		}
		delete[] laser;
		for(unsigned short i=0;i<nbMartiens;i++){
			delete martien[i];
		}
		delete[] martien;
		delete[] tirMartien;
		for(unsigned short i=0;i<nbVenusiens;i++){
			delete venusien[i];
		}
		delete[] venusien;
		delete[] tirVenusien;
		for(unsigned short i=0;i<nbNeptuniens;i++){
			delete neptunien[i];
		}
		delete[] neptunien;
		delete[] tirNeptunien;

		if(nbMartiensMax){gameOver(score,limiteTemps);}

		keybd_event(27, 0, KEYEVENTF_KEYUP, 0);
	}while(difficulte);

	// Fin du programme :
	UIKit::curseurVisible(true);
	system("cls");
	return 0;
}




bool ennemiTouche(Laser* laser,ExtraTerrestre* extraTerrestre,unsigned short* score){
	if(laser->coord.getPositionX()==extraTerrestre->coord.getPositionX()&&laser->coord.getPositionY()==extraTerrestre->coord.getPositionY()){
		laser->killLaser();
		extraTerrestre->isAlive=false;
		extraTerrestre->removeExtraTerrestre();
		*score+=extraTerrestre->ajouterPoints();
		return 1;
	}
	return 0;
}




unsigned int menu(){
	unsigned short choix=0;
	keybd_event(27, 0, 0, 0);
	keybd_event(27, 0, KEYEVENTF_KEYUP, 0);
	do {
		system("cls");
		UIKit::cadre(0,0,LARGEUR_TERRAIN,HAUTEUR_TERRAIN,BLANC);
		UIKit::gotoXY((LARGEUR_TERRAIN-18)/2,6);
		std::cout<<(char)218<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)191;
		//std::cout<<"┌────────────────┐";
		UIKit::gotoXY((LARGEUR_TERRAIN-18)/2,6+1);
		std::cout<</*"│*/(char)179<<" SPACE INVADERS "<<(char)179/*│"*/;
		UIKit::gotoXY((LARGEUR_TERRAIN-18)/2,6+2);
		std::cout<</*"│*/(char)179<<"                "<<(char)179/*│"*/;
		UIKit::gotoXY((LARGEUR_TERRAIN-18)/2,6+3);
		std::cout<</*"│*/(char)179<<"       II       "<<(char)179/*│"*/;
		UIKit::gotoXY((LARGEUR_TERRAIN-18)/2,6+4);
		std::cout<<(char)192<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)196<<(char)217;
		//std::cout<<"└────────────────┘";
		UIKit::gotoXY((LARGEUR_TERRAIN-18)/2,6+4+4);
		std::cout<<"   1 - Niveau 1";
		UIKit::gotoXY((LARGEUR_TERRAIN-18)/2,6+4+5);
		std::cout<<"   2 - Niveau 2";
		UIKit::gotoXY((LARGEUR_TERRAIN-18)/2,6+4+6);
		std::cout<<"   3 - Niveau 3";
		UIKit::gotoXY((LARGEUR_TERRAIN-18)/2,6+4+7);
		std::cout<<"   4 -   Aide";
		UIKit::gotoXY((LARGEUR_TERRAIN-18)/2,6+4+8);
		std::cout<<"   0 -  Quitter";
		UIKit::gotoXY((LARGEUR_TERRAIN-18)/2,6+5+9);
		std::cout<<"     Choix ? ";
		std::cin>>choix;

		if (cin.fail()||cin.peek()!='\n'||choix<0||choix>4){
			cin.clear();
			cin.ignore(512,'\n');
			choix=8;
		}else if(choix==4){
			aide();
			choix=8;
			keybd_event(27, 0, 0, 0);
			keybd_event(27, 0, KEYEVENTF_KEYUP, 0);
		}
		cin.ignore(512,'\n');
	}while(choix<0||choix>3);


	return choix;
}




void aide(){
	system("cls");
	bool test=1;
	Vaisseau ship;
	Timer timerShip;
	timerShip.set(50);
	ship.coord.setPositionX(LARGEUR_TERRAIN/2);
	Laser laser;
	Timer timerLaser;
	timerLaser.set(30);
	laser.isAlive=false;
	UIKit::cadre(0,0,LARGEUR_TERRAIN,HAUTEUR_TERRAIN,BLANC);

	/*UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10);
	std::cout<<"             Synopsis :";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+2);
	std::cout<<"Notre planète est en péril !";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+3+1);
	std::cout<<"Une  armée  de  martiens   a  décidé";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+4+1);
	std::cout<<"d’envahir   la   terre.  Vous   êtes";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+5+1);
	std::cout<<"toutefois le héro de la situation et";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+6+1);
	std::cout<<"la seule personne capable d’anéantir";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+7+1);
	std::cout<<"ces vilains «intragalactiques» ...";*/

	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10);
	std::cout<<"             Synopsis :";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+2);
	std::cout<<"Notre planete est en peril !";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+3+1);
	std::cout<<"Une  armee  de  martiens   a  decide";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+4+1);
	std::cout<<"d envahir   la   terre.  Vous   etes";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+5+1);
	std::cout<<"toutefois le hero de la situation et";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+6+1);
	std::cout<<"la seule personne capable d aneantir";
	UIKit::gotoXY((LARGEUR_TERRAIN-36)/2,10+7+1);
	std::cout<<"ces vilains intragalactiques ...";

	UIKit::gotoXY((LARGEUR_TERRAIN-13)/2,22);
	std::cout<<" Controles :";
	UIKit::gotoXY((LARGEUR_TERRAIN-13)/2,22+2);
	std::cout<<"<D>    Gauche";
	UIKit::gotoXY((LARGEUR_TERRAIN-13)/2,22+3);
	std::cout<<"<A>    Droite";
	UIKit::gotoXY((LARGEUR_TERRAIN-13)/2,22+4);
	std::cout<<"<SPACE> Tirer";
	UIKit::gotoXY((LARGEUR_TERRAIN-13)/2,22+6);
	std::cout<<"<ESC>  Retour";

	while(!GetKeyState(27)||test){
		test=0;
		// Prendre les commandes du vaisseau :
		if(timerShip.isReady()){
			if(GetAsyncKeyState('A')&&UIKit::getCharXY(ship.coord.getPositionX()-1,ship.coord.getPositionY())!=char(186)){
				ship.modifierPosition('k');
			}else if(GetAsyncKeyState('D')&&UIKit::getCharXY(ship.coord.getPositionX()+1,ship.coord.getPositionY())!=char(186)){
				ship.modifierPosition('l');
			}
			if(GetAsyncKeyState(' ')&&!laser.isAlive){
				laser.startLaser(ship.coord.getPositionX());
			}
		}
		// Faire bouger le laser et gérer ses collisions :
		if(timerLaser.isReady()&&laser.isAlive){
			if(laser.coord.getPositionY()==1){
				laser.killLaser();
			}else if(UIKit::getCharXY(laser.coord.getPositionX(),laser.coord.getPositionY()-1)!=32){
				UIKit::gotoXY(laser.coord.getPositionX(),laser.coord.getPositionY()-1);
				std::cout<<(char)32;
				laser.killLaser();
			}else{
				laser.moveLaser();
			}
		}
	}
}




void gameOver(unsigned short score,unsigned short limiteTemps){
	//keybd_event(27, 0, 0, 0);
	//keybd_event(27, 0, KEYEVENTF_KEYUP, 0);

	unsigned short digits;

	system("cls");
	UIKit::gotoXY(0,5);
	std::cout<<
	"   ______   ______   __    __   ______   "<<std::endl<<
	"  /\\  ___\\ /\\  __ \\ /\\ \"-./  \\ /\\  ___\\  "<<std::endl<<
	"  \\ \\ \\__ \\\\ \\  __ \\\\ \\ \\-./\\ \\\\ \\  __\\  "<<std::endl<<
	"   \\ \\_____\\\\ \\_\\ \\_\\\\ \\_\\ \\ \\_\\\\ \\_____\\"<<std::endl<<
	"    \\/_____/ \\/_/\\/_/ \\/_/  \\/_/ \\/_____/"<<std::endl<<
	std::endl<<
	"    ______   __   __ ______   ______   "<<std::endl<<
	"   /\\  __ \\ /\\ \\ / //\\  ___\\ /\\  == \\  "<<std::endl<<
	"   \\ \\ \\/\\ \\\\ \\ \\'/ \\ \\  __\\ \\ \\  __<  "<<std::endl<<
	"    \\ \\_____\\\\ \\__|  \\ \\_____\\\\ \\_\\ \\_\\"<<std::endl<<
	"     \\/_____/ \\/_/    \\/_____/ \\/_/ /_/"<<std::endl;

	UIKit::cadre(0,0,LARGEUR_TERRAIN,HAUTEUR_TERRAIN,BLANC);

	digits=1;
	while(limiteTemps/pow(10,digits)>=1){digits++;}
	UIKit::gotoXY((LARGEUR_TERRAIN-15)/2,20);
	std::cout<<"Temps restant :";
	UIKit::gotoXY((LARGEUR_TERRAIN-digits)/2,22);
	std::cout<<limiteTemps;

	digits=1;
	while(score/pow(10,digits)>=1){digits++;}
	UIKit::gotoXY((LARGEUR_TERRAIN-7)/2,26);
	std::cout<<"Score :";
	UIKit::gotoXY((LARGEUR_TERRAIN-digits)/2,28);
	std::cout<<score;

	UIKit::gotoXY(1,HAUTEUR_TERRAIN-1);
	system("pause");

}