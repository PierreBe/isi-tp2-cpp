#ifndef TIRMARTIEN_H
#define TIRMARTIEN_H

#include "Laser.h"

class TirMartien:public Laser{
public:
	char styleLaser='|';
	void startLaser(unsigned short x, unsigned short y);
	void putLaser() const;
	void moveLaser();
};

#endif